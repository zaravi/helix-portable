{
  description = "Portable Helix configuration";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};

        helixConfig = {
          editor = {
            line-number = "relative";
            scroll-lines = 1;
            lsp = {
              display-messages = true;
              display-inlay-hints = true;
            };
            cursor-shape = {
              insert = "bar";
              normal = "block";
              select = "underline";
            };
            indent-guides = {
              render = true;
              character = "│";
              skip-levels = 0;
            };
          };
          keys.normal = {
            space = {
              space = "file_picker";
              w = ":w";
              q = ":q";
              f = "file_picker_in_current_directory";
              b = "buffer_picker";
              g = "goto_definition";
              h = "hover";
              r = "rename_symbol";
              a = "code_action";
            };
            esc = [ "collapse_selection" "keep_primary_selection" ];
            G = "goto_file_end";
            u = "insert_mode";
            l = "undo";
            L = "undo";
            m = "move_char_left";
            n = "move_visual_line_down";
            e = "move_visual_line_up";
            i = "move_char_right";
          };
          keys.select = {
            l = "undo";
            L = "undo";
            m = "move_char_left";
            n = "move_visual_line_down";
            e = "move_visual_line_up";
            i = "move_char_right";
          };
        };

        # Function to convert a value to a TOML-compatible string
        toTOMLString = value:
          if builtins.isBool value then (if value then "true" else "false")
          else if builtins.isString value then ''"${value}"''
          else if builtins.isList value then "[${builtins.concatStringsSep ", " (map toTOMLString value)}]"
          else if builtins.isInt value || builtins.isFloat value then toString value
          else if builtins.isAttrs value then
            "{\n${builtins.concatStringsSep ",\n" (builtins.attrValues (builtins.mapAttrs (n: v: "  ${n} = ${toTOMLString v}") value))}\n}"
          else "null";

        # Function to process config items recursively
        processConfig = prefix: config:
          builtins.concatStringsSep "\n" (
            builtins.attrValues (builtins.mapAttrs
              (name: value:
                if builtins.isAttrs value then
                  "[${prefix}${name}]\n" + (processConfig "${prefix}${name}." value)
                else
                  "${name} = ${toTOMLString value}"
              )
              config)
          );

        # Create a custom Helix configuration
        customHelixConfig = pkgs.writeTextFile {
          name = "helix-config";
          destination = "/config.toml";
          text = processConfig "" helixConfig;
        };

        # Create a wrapper script for Helix
        helixWrapper = pkgs.symlinkJoin {
          name = "helix-with-tools";
          paths = with pkgs; [
            helix
            marksman
            rust-analyzer
            nil
            rustfmt
            clippy
            cargo
            slint-lsp
            nodePackages.prettier
            nixpkgs-fmt
          ];
          buildInputs = [ pkgs.makeWrapper ];
          postBuild = ''
            wrapProgram $out/bin/hx \
              --set HELIX_RUNTIME "${pkgs.helix}/runtime" \
              --set HELIX_CONFIG_DIR "${customHelixConfig.outPath}" \
              --prefix PATH : ${pkgs.lib.makeBinPath (with pkgs; [
                marksman
                rust-analyzer
                nil
                rustfmt
                clippy
                cargo
                slint-lsp
                nodePackages.prettier
                nixpkgs-fmt
              ])}
          '';
        };

      in
      {
        packages.default = helixWrapper;

        apps.default = flake-utils.lib.mkApp {
          drv = helixWrapper;
          name = "hx";
        };
      }
    );
}
